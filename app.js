var http = require("http");
var url = require("url");
var fs = require("fs");
var qString = require("querystring");

http.createServer(function(req, res){
    if(req.url != "/favicon.ico"){
        var access = url.parse(req.url);
        var html = "";
        var code = "";
        if(access.pathname == "/"){
            code = 200;
            html = "./views/index.html";
            res.writeHead(code, {"Content-Type":"text/html"});
            fs.createReadStream(html).pipe(res);
        }else if(access.pathname == "/profile"){
            code = 200;
            html = "./views/profile.html";
            res.writeHead(code, {"Content-Type":"text/html"});
            fs.createReadStream(html).pipe(res);
        }else if(access.pathname == "/form"){
            if(req.method.toUpperCase() == "POST"){
                var data_post = "";
                req.on("data", function(chunk){
                    data_post += chunk;
                });

                req.on("end", function(){
                    code = 200;
                    html = qString.parse(data_post);
                    console.log(html);

                    res.writeHead(code, {"Content-Type":"application/json; charset=utf-8"});
                    res.end(JSON.stringify(html));
                });
            }else { //GET
                code = 200;
                html = "./views/form.html";
                res.writeHead(code, {"Content-Type":"text/html"});
                fs.createReadStream(html).pipe(res);
            }
        }else if(access.pathname == "/query"){
            code = 200;
            html = qString.parse(access.query);
            res.writeHead(code, {"Content-Type":"application/json; charset=utf-8"});
            res.end(JSON.stringify(html));
        }else {
            code = 404;
            html = "./views/404.html";
            res.writeHead(code, {"Content-Type":"text/html"});
            fs.createReadStream(html).pipe(res);
        }
    }
}).listen(3000);

console.log("Server is running ...");
